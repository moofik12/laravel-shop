<?php

namespace App\Http\Controllers\Admin\Modules;

use App\Category as Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryValidationAjax;
use App\Products;
use App\Services\CatalogService as CatalogService;
use App\Services\LoggingService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class CatalogController extends Controller
{
    /**
     * @var mixed
     */
    protected $catalogService = null;
    /**
     * @var mixed
     */
    protected $loggingService = null;
    /**
     * CatalogController constructor.
     * @param LoggingService $loggingService
     * @param CatalogService $catalogService
     */
    public function __construct(CatalogService $catalogService, LoggingService $loggingService)
    {
        $this->catalogService = $catalogService;
        $this->loggingService = $loggingService;

        /* Логгирование в отдельный лог для текущего файла
         * Логи лежат в директории storage/logs/
         */
        $loggingService->logCurrentClass(__CLASS__);
    }

    public function indexCategories()
    {
        $categories = Category::all()->where('active', 1);

        return view('admin.modules.catalog.categories', ['categories' => $categories]);
    }

    public function indexProducts()
    {
        $activeCategories = $this->catalogService->getActiveCategories();
        $rootCategories   = $this->catalogService->getRootCategory();
        $configuration    = json_encode(Config::get('modules.catalog'));

        return view('admin.modules.catalog.products',
            ['activeCategories' => $activeCategories,
                'rootCategories'    => $rootCategories,
                'configuration'     => $configuration]);
    }

    /*
     * Создает новую категорию продукции
     */
    /**
     * @param  CategoryValidationAjax $request
     * @param  $action
     * @return mixed
     */
    public function addCategory(CategoryValidationAjax $request, $action = null)
    {
        $input = $request->all();

        try {
            $response = $this->catalogService->addCategory($input);
        }
        catch (\Exception $e)
        {
            return $e;
        }

        return response()->json($response);
    }

    /**
     * @param  Request $request
     * @return mixed
     */
    public function addProduct(Request $request)
    {
        $input = $request->all();

        try {
            $this->catalogService->addProduct($input);
        }
        catch (\Exception $e)
        {
            return $e;
        }

    }

    public function deleteProduct(Request $request) {
        $id = $request->input( Config::get('modules.catalog.primarykeys.id') );

        try {
            Products::destroy($id);
        }
        catch (\Exception $e) {

            return $e;
        }
    }

    /**
     * @param Request $request
     */
    public function getCategoryAttributes(Request $request)
    {
        $input = $request->all();
        $categoryId = $input[$this->catalogService->getConfig()->columns->primarykeys->id];

        try {
            $response = $this->catalogService->getCategoryAttributes($categoryId);
        }
        catch (\Exception $e)
        {
            return response()->json($e);
        }

        return response()->json($response);
    }

    /**
     * @param Request $request
     */
    public function getProducts(Request $request)
    {
        $input        = $request->all();
        $idPrimaryKey = $this->catalogService->getConfig()->columns->primarykeys->id;
        $categoryId   = $input[$idPrimaryKey];
        $result       = $this->catalogService->getProductsOfCategory($categoryId);

        return response()->json($result);
    }

    /**
     * Метод возвращающий все подкатегории текущей категории
     * id текущей категории извлекается из $request
     * @param  Request                         $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategories(Request $request)
    {
        $input        = $request->all();
        $idPrimaryKey = $this->catalogService->getConfig()->columns->primarykeys->id;
        $categoryId   = $input[$idPrimaryKey];
        $result       = $this->catalogService->getSubcategoriesById($categoryId);

        return response()->json($result);
    }

    /*
     * This function returns JSONified product data which is response
     * to ajax request received from products page in admin-side.
     * Also may return exception error message.
     *
     * @return mixed
     */
    /**
     * @param Request $request
     */
    public function getProduct(Request $request)
    {
        $input        = $request->all();
        $idPrimaryKey = $this->catalogService->getConfig()->columns->primarykeys->id;
        $productId    = $input[$idPrimaryKey];

        try {
            $productData = $this->catalogService->getProductData($productId);
        }
        catch (\Exception $e)
        {
            return response()->json($e);
        }

        return response()->json($productData);
    }

    /**
     * Обновляет продукт. Возвращает сообщение об ошибке или успешном обновлении.
     * @param  Request                         $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProduct(Request $request)
    {
        $input       = $request->all();
        $productData = $input['msg'];

        try {
            $this->catalogService->updateProduct(json_decode($productData, true));
        }
        catch (\Exception $e)
        {
            return response()->json($e);
        }

        return response()->json('ok');
    }

    /**
     * @param Request $request
     */
    public function getFields(Request $request)
    {
        $input        = $request->all();
        $idPrimaryKey = $this->catalogService->getConfig()->columns->primarykeys->id;
        $categoryId   = $input[$idPrimaryKey];

        try {
            $result = $this->catalogService->getCategoryFields($categoryId);
        }
        catch (\Exception $e)
        {
            return response()->json($e);
        }

        return response()->json($result);
    }

}
