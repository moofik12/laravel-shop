
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

Vue.component('product', require('./components/Product.vue'));

Vue.component('product-update-form', require('./components/ProductUpdateForm.vue'));

Vue.component('products-list', require('./components/ProductsList.vue'));

Vue.component('product-add-form', require('./components/ProductAddForm.vue'));
