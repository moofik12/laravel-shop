<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'test@test.ru',
            'password' => '$2y$10$8Q04Ql6O6Fb3lnEzPuRLau0LLuKgAknmioZmaX/oXVHRq5QHwSYyO',
            'role' => 'admin'
        ]);
    }
}