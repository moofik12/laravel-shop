<header>
    <div id="logo"><img src="img/cd-logo.svg" alt="Homepage"></div>
    <div id="cd-hamburger-menu"><a class="cd-img-replace" href="#0">Menu</a></div>
    <div id="cd-cart-trigger"><a class="cd-img-replace" href="#0">Cart</a></div>
</header>

<nav id="main-nav">
    <ul>
        <li><a class="{{ Request::is('/') ? 'current' : '' }}" href="{{route('home')}}">Главная</a></li>
        <li><a class="{{ Request::is('catalog') ? 'current' : '' }}" href="{{route('front-catalog')}}">Каталог</a></li>
        <li><a class="{{ Request::is('delivery') ? 'current' : '' }}" href="{{route('delivery')}}">Доставка</a></li>
        <li><a class="{{ Request::is('contacts') ? 'current' : '' }}" href="{{route('contacts')}}">Контакты</a></li>
        <li><a class="{{ Request::is('about') ? 'current' : '' }}" href="{{route('about')}}">О нас</a></li>
    </ul>
</nav>

<div id="cd-shadow-layer"></div>

<div id="cd-cart">
    <h2>Корзина</h2>
    <ul class="cd-cart-items">
        <li>
            <span class="cd-qty">1x</span> Product Name
            <div class="cd-price">$9.99</div>
            <a href="#0" class="cd-item-remove cd-img-replace">Remove</a>
        </li>

        <li>
            <span class="cd-qty">2x</span> Product Name
            <div class="cd-price">$19.98</div>
            <a href="#0" class="cd-item-remove cd-img-replace">Remove</a>
        </li>

        <li>
            <span class="cd-qty">1x</span> Product Name
            <div class="cd-price">$9.99</div>
            <a href="#0" class="cd-item-remove cd-img-replace">Remove</a>
        </li>
    </ul> <!-- cd-cart-items -->

    <div class="cd-cart-total">
        <p>Total <span>$39.96</span></p>
    </div> <!-- cd-cart-total -->

    <a href="#0" class="checkout-btn">Подтвердить</a>

    <p class="cd-go-to-cart"><a href="#0">На страницу заказов</a></p>
</div>
