<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    const ADMIN   = 'admin';
    const USER    = 'user';
    const MANAGER = 'manager';

    use AuthenticatesUsers;

    /**
     * @var string
     */
    protected $redirectTo = '/redirectDivide';

    public function __construct()
    {

    }

    /**
     * @param Request $request
     */
    public function redirectDivide(Request $request)
    {
        if ($request->user()->role == self::USER)
        {
            return redirect('/');
        }
        else
        {
            return redirect('/admin');
        }
    }

}
