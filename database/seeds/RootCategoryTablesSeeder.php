<?php

use Illuminate\Database\Seeder;

class RootCategoryTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('catalog_categories')->insert([
            'name' => 'Root Category',
            'description' => 'Root Category',
            'active' => true,
        ]);

        $rootId = DB::table('catalog_categories')->where('name', '=', 'Root Category')->first()->id;
        DB::table('catalog_attribute')->insert([
            'name' => 'Имя',
            'category_id' => $rootId,
            'system_alias' => 'name',
            'type' => 'generic',
        ]);

        DB::table('catalog_attribute')->insert([
            'name' => 'Описание',
            'category_id' => $rootId,
            'system_alias' => 'description',
            'type' => 'text',
        ]);

        DB::table('catalog_attribute')->insert([
            'name' => 'Цена',
            'category_id' => $rootId,
            'system_alias' => 'price',
            'type' => 'int',
        ]);

        DB::table('catalog_attribute')->insert([
            'name' => 'Часть URL',
            'category_id' => $rootId,
            'system_alias' => 'url_part',
            'type' => 'generic',
        ]);

        DB::table('catalog_attribute')->insert([
            'name' => 'Путь до изображения',
            'system_alias' => 'img_path',
            'category_id' => $rootId,
            'type' => 'image',
        ]);

        DB::table('catalog_attribute')->insert([
            'name' => 'Доступность',
            'system_alias' => 'availability',
            'category_id' => $rootId,
            'type' => 'bool',
        ]);

        DB::table('catalog_attribute')->insert([
            'name' => 'Активность',
            'system_alias' => 'active',
            'category_id' => $rootId,
            'type' => 'bool',
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
