<?php

namespace App\Http\Middleware\Admin;

use App\Services\AuthService;
use Closure;

class AclMiddleware
{

    /**
     * @var mixed
     */
    protected $checker;

    /**
     * @param AuthService $checker
     */
    public function __construct(AuthService $checker)
    {
        $this->checker = $checker;
    }

    /**
     * @param $request
     * @param Closure $next
     * @param $roles
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if (!$this->checker->checkRole($roles))
        {
            return redirect('404');
        }

        return $next($request);
    }

}
