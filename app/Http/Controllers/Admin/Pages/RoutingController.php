<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Http\Controllers\Controller;

class RoutingController extends Controller
{
    
    public function index()
    {
       	return view('admin.routing');
    }
}