<?php
namespace App\Helpers\Primitives;

class Helper {
    public static function strpos_arr($haystack, $needle) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $what) {
            if ( ($pos = strpos($haystack, $what)) !==false ) return $pos;
        }
        return false;
    }

    public static function convert_value_from_string($string) {
        if($string == 'true') {
            return true;
        }
        else if($string == 'false') {
            return false;
        }
    }
}