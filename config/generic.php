<?php

return [
    'base' => [
        'counter' => 1
    ],

	'folders' => [
		'images' => 'images',
	],

	'mounts' => [
		'uploaded' => 'uploaded',
		'local' => 'local',
		's3' => 's3',
		'public' => 'public',
	],

	'separators' => [
		'dot' => '.',
		'ds' => '/',
	],

	'stringnames' => [
		'file' => 'file'
	],
];