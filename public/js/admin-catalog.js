/**
 * Created by Sasha on 20.05.2017.
 */
var catalogRoutes = {
    productsRetrieve : "",
    categoriesRetrieve: "",
    productsAttrsRetrieve: "",
    productsAdd: "",
    productDataRetrieve : ""
}

// function productsRetrieve(catId) {
//     $.ajax({
//         url: catalogRoutes.productsRetrieve,
//         method: "POST",
//         data: {
//             'id' : catId,
//             '_token': $('meta[name="csrf-token"]').attr('content'),
//         },
//         dataType: "json",
//         success: function( msg ) {
//             $('#products-container').append('' +
//                 '<div class="top-buffer block-title-row">Список товаров: </div>');
//             for(var key in msg) {
//                 $('#products-container').append(
//                     '<a class="' + key + '" data-product="true" href="#">' + msg[key] + "</a>"
//                 );
//             }
//         },
//         error: function( jqXHR, textStatus ) {
//             console.log( "Request failed: " + jqXHR );
//         }
//     });
// }

function acnhorClickListener() {
    // $('#products-list').click(function (e) {
    //     e.preventDefault();
    //     if(e.target.tagName != 'A') {
    //         return;
    //     }
    //     if(e.target.dataset.product == 'true') {
    //         formConstructor.makeEditProductForm($(e.target).attr('class'));
    //         return;
    //     }
    //
    //     var catId = $(e.target).attr('class');
    //     $.ajax({
    //         url: catalogRoutes.categoriesRetrieve,
    //         method: "POST",
    //         data: {
    //             'id' : catId,
    //             '_token': $('meta[name="csrf-token"]').attr('content'),
    //         },
    //         dataType: "json",
    //         success: function( msg ) {
    //             console.log(msg);
    //             $('#products-container').html('');
    //             for(var key in msg) {
    //                 $('#products-container').append(
    //                     '<br/><a  href="#" class="'+ key + '">' + msg[key] + '</a>'
    //                 );
    //             }
    //             productsRetrieve(catId);
    //         },
    //         error: function( jqXHR, textStatus ) {
    //             $('#products-container').html('');
    //             console.log( "Request failed: " + jqXHR );
    //             productsRetrieve(catId);
    //         }
    //     });
    //
    //
    // });
}


catalogRoutes.handleInteraction = function () {
    acnhorClickListener();
    $('#category').change(
        function (e) {
            $('#category-fields').html('');
            var id = $(this).val();
            $.ajax({
                url: catalogRoutes.productsAttrsRetrieve,
                method: "POST",
                data: {
                    'id' : id,
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                },
                dataType: "json",
                success: function( msg ) {
                    for(var key in msg) {
                        $('#category-fields').append('<div class="form-group"><label for="'
                            + key + '">'+ msg[key]
                            + '</label><input type="text" class="form-control" id="'
                            + key + '"></div>'
                        );
                    }
                },
                error: function( jqXHR, textStatus ) {
                    $('#category-fields').html('');
                }
            });
        }
    );

    $('#add-product').click(function () {
        $('#products-list').prop('hidden', true);
        $('#add-product').css('display', 'none');
        $('#products-add-form').prop('hidden', false);
        $('#add-product-submit').css('display', 'block');
    });

    $('#add-product-submit').click(function () {
        $('#products-list').prop('hidden', false);
        $('#add-product').css('display', 'block');
        $('#products-add-form').prop('hidden', true);
        $('#add-product-submit').css('display', 'none');

        var category_id = $( "#category option:selected" ).val();
        var name = $( "#name" ).val();
        var price = $( "#price" ).val();
        var description = $( "#description" ).val();
        var url_part = $( "#url_part" ).val();
        var img_path = $( "#img_path" ).val();
        var active = $( "#active" ).is(":checked");
        var availability = $( "#availability" ).is(":checked");

        var $additionalFields = $('#category-fields input');

        var additionalKeys = $additionalFields.map(function(){
            return this.id;
        }).get();

        var additionalValues = $additionalFields.map(function(){
            return $( this ).val();
        }).get();

        var additional = arrayCombine(additionalKeys, additionalValues);

        var action = "add";

        $.ajax({
            url: catalogRoutes.productsAdd,
            method: "POST",
            data: {
                'name' : name,
                'category_id' : category_id,
                'price' : price,
                'description' : description,
                'url_part' : url_part,
                'img_path' : img_path,
                'active' : active,
                'availability' : availability,
                'additional' : additional,
                '_token': $('meta[name="csrf-token"]').attr('content'),
            },
            dataType: "text",
            success: function( msg ) {
                console.log(msg);
                document.getElementById('products-add').reset();
                $('#category-fields').html('');
            },
            error: function( jqXHR, textStatus ) {
                console.log( "Request failed: " + jqXHR );
            }
        });
    });
}

var formConstructor = {
    makeEditProductForm : function (product_id) {
        helper.setObservable(this);
        helper.getProductData(product_id);
    },
    notify: function (code) {
        if(code == MessageContainer.SUCCESS) {
            console.log(MessageContainer.data);
            // var $productForm = $('.product-form');
            // $productForm.css("display", "block");
            // $productForm.fadeIn();
        }
        else {
            alert('error');
        }
    }
}

var helper = {
    setObservable : function(observable) {
        this.observable = observable;
    },
    getProductData : function (product_id) {
        var observable = this.observable;
        $.ajax({
            url: catalogRoutes.productDataRetrieve,
            method: "POST",
            data: {
                'id' : product_id,
                '_token': $('meta[name="csrf-token"]').attr('content'),
            },
            dataType: "json",
            success: function( msg ) {
                MessageContainer.data = msg;
                observable.notify(MessageContainer.SUCCESS);
            },
            error: function( jqXHR, textStatus ) {
                console.log( "Request failed: " + jqXHR );
                observable.notify(MessageContainer.FAIL);
            }
        });
    }
}

var MessageContainer = {
    FAIL : '0',
    SUCCESS : '1'
}