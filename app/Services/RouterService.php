<?php

namespace App\Services;
use App\Helpers\Primitives\Helper;

class RouterService {
    private $routesDir = '';
    private $spathRoutesDir = '';
    private $routesPrefixes = [];
    private static $instance;

    public function __construct($path = 'routes/')
    {
        $this->routesDir = $_SERVER['DOCUMENT_ROOT'] . '/../' . $path;
        $this->spathRoutesDir = $path;
    }


    public function registerPrefix($prefix)
    {
        if(is_array($prefix)) {
            $this->routesPrefixes = array_merge($this->routesPrefixes, $prefix);
        }
        else {
            $this->routesPrefixes[] = $prefix;
        }
    }


    public function find() {
        $files = scandir($this->routesDir);
        $routes = [];

        foreach ($files as $file) {
            if( ($pos = Helper::strpos_arr($file, $this->routesPrefixes)) !== false && $pos == 0 ) {
                $routes[] = $this->spathRoutesDir . $file;
            }
        }

        return $routes;
    }
}