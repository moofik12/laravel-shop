<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;

class AuthService {

    public function __construct() {
        
    }

    public function checkRole($roles = []) {
        $user = Auth::user();
        if ($user && in_array($user->role, $roles)) {
            return true;
        }
        return false;
    }

}
