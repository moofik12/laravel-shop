<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColsFromProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalog_products', function (Blueprint $table) {
            $table->dropColumn([
                'name', 'description', 'price',
                'url_part', 'img_path', 'availability',
                'active'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalog_products', function (Blueprint $table) {
            $table->string('name');
            $table->text('description');
            $table->integer('price');
            $table->string('url_part')->unique();
            $table->string('img_path')->nullable();
            $table->boolean('availability');
            $table->boolean('active');
        });
    }
}
