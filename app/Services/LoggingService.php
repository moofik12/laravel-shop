<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 08.06.2017
 * Time: 16:55
 */

namespace App\Services;
use Illuminate\Support\Facades\Log;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LoggingService {
    private function fetchName($class) {
        $logFileName = explode('\\', $class);
        if(is_array($logFileName)) {
            $logFileName = end($logFileName);
        }
        return 'logs/' . $logFileName . '.log';
    }

    public function makeInfoStreamHandler($class) {
        $filename = $this->fetchName($class);
        $infoStreamHandler = new StreamHandler(
            storage_path($filename), Logger::INFO, false);
        return $infoStreamHandler;
    }

    public function logCurrentClass($class) {
        $monolog = Log::getMonolog();
        $monolog->pushHandler($this->makeInfoStreamHandler($class));
    }
}