<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attributes extends Model
{
    protected $table = "catalog_attribute";
    protected $fillable = ['name', 'category_id', 'type'];
}
