<?php
namespace App\Services;

use App\Attributes;
use App\AttributesValues;
use App\Category;
use App\Helpers\Primitives\Helper;
use App\Products;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CatalogService
{

    /**
     * @var mixed
     */
    private $rootCategory;
    /**
     * @var array
     */
    private $activeCategories = [];
    /**
     * @var array
     */
    private $configuration = [];

    public function __construct()
    {
        $this->configuration = Config::get('modules.catalog');
        $this->rootCategory  = $this->configuration['init']['root_category_id'];
    }

    /**
     * @return mixed
     */
    public function getRootCategory()
    {
        return $this->rootCategory;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        $configuration = json_decode(json_encode($this->configuration));

        return $configuration;
    }

    /**
     * @return mixed
     */
    public function getActiveCategories()
    {
        if (empty($this->activeCategories))
        {
            $allCategories = Category::all();
            foreach ($allCategories as $cat)
            {
                if ($cat->active == true &&
                    $cat->id != $this->configuration['init']['root_category_id'])
                {
                    $this->activeCategories[] = $cat;
                }
            }
        }

        return $this->activeCategories;
    }

    /**
     * @param  $categoryId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function getProductsOfCategory($categoryId)
    {
        $lockedPrefixes = ['attribute_id', 'created_at', 'updated_at'];

        $products = DB::table($this->configuration['tables']['products'])->
            join($this->configuration['tables']['attrs_values'], function ($join) use ($categoryId)
        {
            $join->on(
                field(
                    $this->configuration['tables']['products'],
                    $this->configuration['columns']['primarykeys']['id']
                ),
                '=',
                field(
                    $this->configuration['tables']['attrs_values'],
                    $this->configuration['columns']['foreignkeys']['product'])
            )
                ->where(
                    field(
                        $this->configuration['tables']['products'],
                        $this->configuration['columns']['foreignkeys']['category']
                    ), '=', $categoryId);
        })->
            join($this->configuration['tables']['attrs'],
            field(
                $this->configuration['tables']['attrs'],
                $this->configuration['columns']['primarykeys']['id']
            ), '=',
            field(
                $this->configuration['tables']['attrs_values'],
                $this->configuration['columns']['foreignkeys']['attr'])
        )
            ->select(
                field(
                    $this->configuration['tables']['products'],
                    $this->configuration['columns']['primarykeys']['id'])
                . alias($this->configuration['columns']['foreignkeys']['product']),
                field($this->configuration['tables']['attrs_values'], '*'),
                field($this->configuration['tables']['attrs'], 'name') . alias('attr_name'),
                field($this->configuration['tables']['attrs'], 'type'),
                field($this->configuration['tables']['attrs'], 'system_alias')
            )->get()->all();

        $productArr = [];
        foreach ($products as $product)
        {
            foreach ($product as $key => $value)
            {
                if (Helper::strpos_arr($key, $lockedPrefixes) === false && $value != '')
                {
                    if (strpos($key, $this->configuration['columns']['basecols']['value']) !== false &&
                        $value != '')
                    {
                        $productArr[$product->product_id]
                        [$this->configuration['specials']['prefixes']['attr'] . $product->attribute_id]
                        [$this->configuration['columns']['basecols']['value']] = $value;
                    }
                    else
                    {
                        $productArr[$product->product_id]
                        [$this->configuration['specials']['prefixes']['attr'] . $product->attribute_id]
                        [$key] = $value;
                    }
                }

            }
        }

        return $productArr;
    }

    /**
     * Возвращает список подкатегорий, текущей категории
     * @param  $categoryId                         - id текущей категории
     * @return array|\Illuminate\Http\JsonResponse - массив вида $id => имя_категории
     */
    public function getSubcategoriesById($categoryId)
    {
        $categories = Category::where('parent_id', $categoryId)->get();
        if ($categories == null)
        {
            return null;
        }

        $result = [];
        foreach ($categories as $category)
        {
            $result[$category->id] = $category->name;
        }

        return $result;
    }

    /**
     * Получает данные о продукта по id продукта
     * @param  $productId - id продукта
     * @return array|null - массив с данными о продукте
     */
    public function getProductData($productId)
    {
        // this prefixes will be marked as bound with Attribute and Value tables
        $lockedPrefixes = ['value', 'attr'];
        $specialAttrs   = [];

        $product = DB::table($this->configuration['tables']['products'])->
            join($this->configuration['tables']['attrs_values'], function ($join) use ($productId)
        {
            $join->on(
                field(
                    $this->configuration['tables']['products'],
                    $this->configuration['columns']['primarykey']['id']
                ),
                '=',
                field(
                    $this->configuration['tables']['attrs_values'],
                    $this->configuration['columns']['foreignkeys']['product']
                )
            )->
                where(
                field(
                    $this->configuration['tables']['products'],
                    $this->configuration['columns']['primarykeys']['id']
                ), '=', $productId
            );
        })->
            join($this->configuration['tables']['attrs'],
            field(
                $this->configuration['tables']['attrs'],
                $this->configuration['columns']['primarykeys']['id']
            ), '=',
            field(
                $this->configuration['tables']['attrs_values'],
                $this->configuration['columns']['foreignkeys']['attr']
            )
        )->
            select(
            field($this->configuration['tables']['products'], '*'),
            field($this->configuration['tables']['attrs_values'], '*'),
            field
            (
                $this->configuration['tables']['attrs'],
                $this->configuration['columns']['basecols']['name']
            )
            . alias(
                $this->configuration['specials']['prefixes']['attr']
                . $this->configuration['columns']['basecols']['name']
            )
        )->get()->all();

        $productObj          = [];
        $attributeIdentifier = $this->configuration['columns']['foreignkeys']['attr'];

        if ($product[0] != null)
        {
            foreach ($product[0] as $property => $value)
            {
                try {
                    if (Helper::strpos_arr($property, $lockedPrefixes) === false)
                    {
                        $productObj[$property] = $value;
                    }
                    else
                    {
                        $specialAttrs[] = $property;
                    }
                }
                catch (\Exception $e)
                {
                    Log::info('ERROR IN SERVICE LAYER: ' . $e);

                    return null;
                }

            }
        }

        foreach ($product as $entity)
        {
            $attrSetKey = 'attribute_id_' . $entity->$attributeIdentifier;
            foreach ($specialAttrs as $attr)
            {
                if ($entity->$attr != null)
                {
                    if (strpos($attr, $this->configuration['columns']['basecols']['value']) !== false)
                    {
                        $productObj[$attrSetKey][$this->configuration['columns']['basecols']['value']]
                        = $entity->$attr;
                    }
                    else
                    {
                        $productObj[$attrSetKey][$attr] = $entity->$attr;
                    }
                }
            }
        }

        return $productObj;
    }

    /**
     * @param $productData
     */
    public function updateProduct($productData)
    {
        foreach ($productData as $attr => $properties)
        {
            AttributesValues::find($properties[$this->configuration['columns']['primarykeys']['id']])
                ->update([$this->getValueType($properties[
                    $this->configuration['columns']['basecols']['type']
                ]) => $properties[$this->configuration['columns']['basecols']['value']]]);
        }

        return null;
    }

    /**
     * @param $type
     * @return mixed
     */
    private function getValueType($type)
    {
        switch ($type)
        {
        case 'bool':
            $typeVal = 'value_bool';
            break;
        case 'text':
            $typeVal = 'value_text';
            break;
        case 'date':
            $typeVal = 'value_date';
            break;
        case 'int':
            $typeVal = 'value_int';
            break;
        default:
            $typeVal = 'value_generic';
            break;
        }

        return $typeVal;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAttributeType($id)
    {
        $attr = Attributes::where('id', $id)->get()->first();

        return $this->getValueType($attr->type);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCategoryFields($id)
    {
        $idCol   = $this->configuration['columns']['primarykeys']['id'];
        $nameCol = $this->configuration['columns']['basecols']['name'];
        $typeCol = $this->configuration['columns']['basecols']['type'];

        while (true)
        {
            $category       = Category::where($idCol, $id)->get()->first();
            $categoryTree[] = $category->$idCol;
            $id             = $category->parent_id;
            if ($id == null)
            {
                break;
            }
        }

        foreach ($categoryTree as $categoryId)
        {
            $attrs = Attributes::where($this->configuration['columns']['foreignkeys']['category'], $categoryId)
                ->get()->all();
            foreach ($attrs as $attr)
            {
                $attrSetKey = $this->configuration['specials']['prefixes']['attr'] . $attr->$idCol;
                $result[$attrSetKey][$this->configuration['specials']['prefixes']['attr'] . $nameCol]
                                               = $attr->$nameCol;
                $result[$attrSetKey][$typeCol] = $attr->$typeCol;
            }
        }

        return $result;
    }

    /**
     * @param $type
     * @return mixed
     */
    public function getFieldsByType($type)
    {
        $rootCategoryId = Config::get('modules.catalog.init.root_category)');

        $attrs = Attributes::where(
            $this->configuration['columns']['foreignkeys']['category'], $this->rootCategory
        )
            ->where(
                $this->configuration['columns']['basecols']['type'], $type
            )
            ->get()->all();

        return $attrs;
    }

    /**
     * @param $input
     */
    public function addCategory($input)
    {
        $category = new Category;
        if ($input['parent_id'] != '' && $input['parent_id'] != null)
        {
            $category->parent_id = $input['parent_id'];
        }
        $category->name        = $input['name'];
        $category->description = $input['description'];
        $category->active      = ($input['active'] == 'true' ? true : false);
        $category->url_part    = $input['url_part'];
        $category->save();

        $additionalAttrs = isset($input['additional']) ? json_decode($input['additional']) : [];
        foreach ($additionalAttrs as $attribute)
        {
            $attr               = new Attributes;
            $attr->category_id  = $category->id;
            $attr->name         = $attribute->name;
            $attr->type         = $attribute->type;
            $attr->system_alias = $attribute->system_alias;
            $attr->save();
        }

        return [
            'status' => 'ok',
            'id'     => $category->id,
            'name'   => $category->name,
        ];
    }

    /**
     * @param $input
     */
    public function addProduct($input)
    {
        $counter            = 1;
        $mount              = Config::get('generic.mounts.uploaded');
        $folderToSave       = Config::get('generic.folders.images');
        $directorySeparator = Config::get('generic.separators.ds');
        $imageType          = $this->configuration['types']['image'];
        $imagePrefix        = $this->configuration['specials']['prefixes']['image'];
        $attributePrefix    = $this->configuration['specials']['prefixes']['attr'];
        $categoryId         = $this->configuration['columns']['foreignkeys']['category'];

        $type        = $this->getFieldsByType($imageType);
        $imageAttrId = $type[0]['id'];

        $product              = new Products();
        $product->$categoryId = $input['category'];
        $product->save();
        $productId = $product->id;

        foreach ($input as $field => &$value)
        {
            if (strpos($field, $attributePrefix) !== false)
            {
                $attrId                    = substr($field, strpos($field, '_') + 1);
                $attrsValues               = new AttributesValues();
                $attrsValues->product_id   = $productId;
                $attrsValues->attribute_id = $attrId;
                $valueField                = $this->getAttributeType($attrId);
                if ($valueField == 'value_bool')
                {
                    $value = Helper::convert_value_from_string($value);
                }
                $attrsValues->$valueField = $value;
                $attrsValues->save();
            }
            else
            if (strpos($field, $imagePrefix) !== false)
            {
                $file         = request()->file($field);
                $relativePath = $file->storeAs(
                    $folderToSave,
                    uniqid() . '.' . $file->extension(),
                    $mount
                );
                ++$counter;
                $pathes[] = $directorySeparator . $mount . $directorySeparator . $relativePath;
            }
        }

        $valueField                = $this->getAttributeType($imageAttrId);
        $attrsValues               = new AttributesValues();
        $attrsValues->product_id   = $productId;
        $attrsValues->attribute_id = $imageAttrId;
        if (!empty($pathes))
        {
            $attrsValues->$valueField = end($pathes);
        }
        $attrsValues->save();
    }

    /**
     * Получает атрибуты определенной категории
     * (включая атрибуты родительских для нее категорий)
     * @param $input - запрос
     * @return array - массив, содержащий атрибуты заданой категории
     */
    public function getCategoryAttributes($categoryId)
    {
        $idPrimaryKey       = $this->configuration['columns']['primarykeys']['id'];
        $categoryForiegnKey = $this->configuration['foreignkeys']['category'];
        $attributes      = Attributes::where($categoryForiegnKey, $categoryId)->get();

        $col_result = [];
        while (true)
        {
            foreach ($attributes as $attr)
            {
                $col_result[$attr->id] = $attr->name;
            }

            $categoryId = Category::where($idPrimaryKey, $categoryId)->first()->parent_id;
            if ($categoryId != null)
            {
                $attributes = Attributes::where($categoryForiegnKey, $categoryId)->get();
            }
            else
            {
                break;
            }
        }

        return $col_result;
    }

}

if (!function_exists('field'))
{

    /**
     * Делает ссылку на поле таблицы в формате SQL
     * @param $table - таблица
     * @param $field - поле таблицы
     * @return $string - строка формата 'table.field'
     */
    function field($table, $field)
    {
        return $table . '.' . $field;
    }
}

if (!function_exists('alias'))
{

    /**
     * Задает псевдоним для атрибута в SQL запросе
     * @param $attrName - часть SQL запроса содержащая псевдоним для поля
     */
    function alias($attrName)
    {
        return ' AS ' . $attrName;
    }
}
