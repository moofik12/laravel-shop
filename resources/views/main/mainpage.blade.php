@extends('layouts.master')
@section('title')Продажа оригинальной продукции Apple
@endsection

@section('content')
<div class="main-page-container">
    <div class="container"> 
        <div class="row"> 
            <div class="col-xs-12">
                <h1 class="text-center shop-text-title h1"> 
                    Интернет-магазин продукции Apple
                </h1> 

                <h1 class="text-center h2"> 
                    Продажа смартфонов, планшетов и аксессуаров фирмы Apple 
                </h1>
                <main>
                    <ul id="cd-gallery-items" class="cd-container">
                        <li class="text-center">
                            <div class="thumbnail">
                                <span class="glyphicon glyphicon-plane shop-main-icons" aria-hidden="true"></span>
                                <div class="caption">
                                    <h3>
                                        Доставка в ближайшие магазины вашего города
                                    </h3>
                                </div>
                            </div>
                        </li>
                        <li class="text-center">
                            <div class="thumbnail">
                                <span class="glyphicon glyphicon-home shop-main-icons" aria-hidden="true"></span>
                                <div class="caption">
                                    <h3>
                                        Широкий выбор доступных магазинов
                                    </h3>
                                </div>
                            </div>
                        </li>
                        <li class="text-center">
                            <div class="thumbnail">
                                <span class="glyphicon glyphicon-credit-card shop-main-icons" aria-hidden="true"></span>
                                <div class="caption">
                                    <h3>
                                        Наличный и безналичный расчет
                                    </h3>
                                </div>
                            </div>
                        </li>
                        <li class="text-center">
                            <div class="thumbnail">
                                <span class="glyphicon glyphicon-search shop-main-icons" aria-hidden="true"></span>
                                <div class="caption">
                                    <h3>
                                        Удобная система поиска товаров
                                    </h3>
                                </div>
                            </div>
                        </li>
                        <li class="text-center">
                            <div class="thumbnail">
                                <span class="glyphicon glyphicon-usd shop-main-icons" aria-hidden="true"></span>
                                <div class="caption">
                                    <h3>
                                        Возможность сравнения цен товаров
                                    </h3>
                                </div>
                            </div>
                        </li>
                        <li class="text-center">
                            <div class="thumbnail">
                                <span class="glyphicon glyphicon-king shop-main-icons" aria-hidden="true"></span>
                                <div class="caption">
                                    <h2>
                                        Самый удобный сервис
                                    </h2>
                                </div>
                            </div>
                        </li>           
                    </ul> <!-- cd-gallery-items -->
                </main>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('/js/main.js')}}"></script>
@endsection
