<?php

//admin part

Route::group(['middleware' => ['acl:admin,manager']], function () {

    Route::get('/admin', 'Admin\BackendController@index')->name('admin');
    Route::get('/admin/routing', 'Admin\Pages\RoutingController@index')->middleware('acl:admin')->name('routing');

    //Выводит страницу со списком всех категорий
    Route::get('/admin/modules/catalog', 'Admin\Modules\CatalogController@indexCategories')
            ->name('catalog');

    //Выводит страницу со списком всех продуктов
    Route::get('/admin/modules/products', 'Admin\Modules\CatalogController@indexProducts')
            ->name('products');

    //Создает новую категорию продукции
    Route::post('/admin/modules/catalog/ajax/{action?}', 'Admin\Modules\CatalogController@addCategory')
            ->name('addCategory');

    //Достает атрибуты определенной категории продуктов
    Route::post('/admin/modules/products/ajax/attrs', 'Admin\Modules\CatalogController@getCategoryAttributes')
            ->name('getCategoryAttributes');

    //Добавляет новый продукт
    Route::post('/admin/modules/products/ajax/add/product', 'Admin\Modules\CatalogController@addProduct')
            ->name('addProduct');

    //Достет список продуктов категории
    Route::post('/admin/modules/products/ajax/retrieve/products', 'Admin\Modules\CatalogController@getProducts')
            ->name('getProducts');

    //Достает список категорий
    Route::post('/admin/modules/products/ajax/retrieve/cats', 'Admin\Modules\CatalogController@getCategories')
            ->name('getCategories');

    //Достает данные по конкретному продукту
    Route::post('/admin/modules/products/ajax/retrieve/product', 'Admin\Modules\CatalogController@getProduct')
            ->name('getProduct');

    Route::post('/admin/modules/products/ajax/update/product', 'Admin\Modules\CatalogController@updateProduct')
        ->name('updateProduct');

    Route::post('/admin/modules/products/ajax/delete/product', 'Admin\Modules\CatalogController@deleteProduct')
        ->name('deleteProduct');

    Route::post('/admin/modules/products/ajax/retrieve/fields', 'Admin\Modules\CatalogController@getFields')
        ->name('getFields');

});
