
Vue.config.devtools = true;
const vm = new Vue({
    el: '#catalog-container',
    data: {
        productModal: false,
        addForm: false,
        submitted: false,
        successMessage: ''
    },
    methods: {
        showAddForm: function () {
            this.addForm = true;
        }
    },
    created: function () {
        var vm = this;
        Event.$on('productAdded', function (offerName) {
            vm.addForm = false;
            vm.submitted = true;
            vm.successMessage = offerName;
        })
    }
});
