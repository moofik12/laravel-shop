@extends('layouts.admin')

@section('nav-title')
    Admin Panel
@endsection

@section('content')

    <div id="catalog-container" class="container">

        <h3>Каталог: товары</h3>
        <div class="row no-margins">
            <button @click="showAddForm()" id="add-product" type="button" class="btn btn-default btn-lg">
                <span class="glyphicon glyphicon-plus"></span> Добавить новый товар
            </button>
        </div>

        <div class="row no-margins">
            <div v-if="submitted" class="alert alert-success fade in alert-dismissable col-lg-6" style="margin-top:18px;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                Товар <strong>@{{ successMessage }}</strong> добавлен!
            </div>
        </div>

        <div class="row no-margins">
            <products-list v-if="!addForm"></products-list>
            <product-add-form v-if="addForm"></product-add-form>
            <product-update-form></product-update-form>
        </div>

@endsection

@section('scripts')

    <script>
        window.Event = new Vue();
        window.Cache = {};
        window.Store = {
            variables: {
                activeCategories: {!! json_encode($activeCategories) !!}
            },
            routes: {
                addProduct: '{{route('addProduct')}}',
                updateProduct: '{{route('updateProduct')}}',
                getCategories: '{{route('getCategories')}}',
                getProducts: '{{route('getProducts')}}',
                getCategoryFields: '{{route('getFields')}}',
                deleteProduct: '{{route('deleteProduct')}}'
            }
        };
        window.Config = {!! json_encode(config('modules.catalog')) !!};
    </script>

    <script src="{{asset('/js/helper.js')}}"></script>
    <script src="{{asset('/js/admin-catalog.js')}}"></script>
    <script src="{{asset('/js/products.js')}}"></script>

@endsection


