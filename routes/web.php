<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//pages
Route::get('/', 'PagesController@main')->name('home');

//catalog 
Route::get('/catalog', 'PagesController@catalog')->name('front-catalog');

//delivery 
Route::get('/delivery', 'PagesController@delivery')->name('delivery');

//contacts 
Route::get('/contacts', 'PagesController@contacts')->name('contacts');

//about 
Route::get('/about', 'PagesController@about')->name('about');

//auth
Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/redirectDivide', 'Auth\LoginController@redirectDivide');
