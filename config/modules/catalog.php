<?php

return [
    'columns' => [
        'foreignkeys' => [
            'product' => 'product_id',
            'attr' => 'attribute_id',
            'category' => 'category_id'
        ],

        'primarykeys' => [
            'id' => 'id'
        ],

        'basecols' => [
            'value' => 'value',
            'alias' => 'system_alias',
            'name' => 'name',
            'type' => 'type'
        ]
    ],

    'tables' => [
        'categories' => '',
        'products' => 'catalog_products',
        'attrs_values' => 'catalog_attribute_value',
        'attrs' => 'catalog_attribute'
    ],

    'units' => [
        'product' => 'product',
        'category' => 'category',
        'attribute' => 'attr',
    ],

    'types' => [
        'bool' => 'bool',
        'text' => 'text',
        'int' => 'int',
        'date' => 'date',
        'image' => 'image',
        'string' => 'string',
        'generic' => 'generic',
    ],

    'specials' => [
        'prefixes' => [
            'attr' => 'attr_',
            'file' => 'file_',
            'image' => 'image_'
        ],
    ],

    'init' => [
        'root_category_id' => 1,

        'baseattrs' => [
            'name' => 'attr_1',
            'description' => 'attr_2',
            'price' => 'attr_3',
            'url' => 'attr_4',
            'file' => 'attr_5'
        ],

        'settings' => [
            'max_images_count' => 3
        ]
    ]
];