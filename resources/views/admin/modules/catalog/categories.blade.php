@extends('layouts.admin')

@section('nav-title')
Admin Panel
@endsection

@section('content')
<div id="catalog-container" class="container category">
    <div class="row no-margins">
        <div v-if="submitted" class="alert alert-success fade in alert-dismissable col-lg-6" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            Категория <strong>@{{ successMessage }}</strong> создана!
        </div>
    </div>    
    <h3>Каталог: категории</h3>
    <div class="row no-margins">
        <button v-show="!isShow" v-on:click="showAdd" type="button" class="btn btn-default btn-lg">
            <span class="glyphicon glyphicon-plus"></span> Добавить новую категорию
        </button>
    </div>
    <div class="row no-margins">
        <div id="categories-list" v-show="!isShow" class="col-lg-6 block-bordered top-buffer bg-white">  
            <h3>Категории</h3>
            <hr>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            @endif
            <categories-list></categories-list>
        </div>
        <div id="categories-add-form" style="display: none" v-show="isShow" class="col-lg-6 block-bordered top-buffer bg-white">
            <div class="block-title-row">Добавить категорию</div>           
            <form class="top-buffer bottom-buffer">
                <div class="form-group" v-bind:class="{'has-error': errors.name }">
                    <label for="name">Имя категории:</label>
                    <input v-model="categoryName" type="text" class="form-control" id="name">
                    <form-error v-if="errors.name" v-bind:errors="errors">
                        @{{ errors.name[0] }}
                    </form-error>
                </div>
                <div class="form-group" v-bind:class="{'has-error': errors.parent_id }">
                    <label for="parent_id">Родительская категория:</label>
                    <select v-model="categoryParentId" class="form-control" id="parent_id">
                        @foreach ($categories as $category)
                        <option value="{{ $category->id }}" >{{ $category->name }}</option>
                        @endforeach
                    </select>
                    <form-error v-if="errors.parent_id" v-bind:errors="errors">
                        @{{ errors.parent_id[0] }}
                    </form-error>
                </div>
                <div class="form-group" v-bind:class="{'has-error': errors.description }">
                    <label for="description">Описание</label>
                    <textarea v-model="categoryDescription" class="form-control" id="description"></textarea>
                    <form-error v-if="errors.description" v-bind:errors="errors">
                        @{{ errors.description[0] }}
                    </form-error>
                </div>
                <div class="form-group" v-bind:class="{'has-error': errors.url_part }">
                    <label for="url_part">Отображение в URL</label>
                    <input v-model="categoryUrlPart" type="text" class="form-control" id="url_part">
                    <form-error v-if="errors.url_part" v-bind:errors="errors">
                        @{{ errors.url_part[0] }}
                    </form-error>
                </div>
                <div class="checkbox">
                    <label><input v-model="categoryActive" id="active" type="checkbox"> Показывать на сайте</label>
                </div>
                <div v-for="attribute in attributes" id="new-fields-container">                    
                    <div class="form-group new-category-field">
                        <label for="attr_name">Название аттрибута</label>
                        <input v-model="attribute.name" type="text" class="form-control" id="attr_name" data-type="name">
                    </div>
                    <div class="form-group new-category-field">
                        <label for="attr_type">Тип</label>
                        <select v-model="attribute.type" class="form-control" id="attr_type" data-type="type">
                            <option disabled value="">Выберите тип значения поля</option>
                            <option value="generic">Общий</option>
                            <option value="generic">Строка</option>
                            <option value="bool">Булев тип</option>
                            <option value="int">Целое</option>
                        </select>
                        {{--<input v-model="attribute.type" type="text" class="form-control" id="attr_type" data-type="type">--}}
                    </div>
                    <div class="form-group new-category-field">
                        <label for="system_alias">Системный алиас</label>
                        <input v-model="attribute.system_alias" type="text" class="form-control" id="system_alias" data-type="type">
                    </div>
                </div> 
                <button v-on:click="addNewField" id="add-new-field" type="button" class="btn btn-default btn-lg top-buffer">
                    <span class="glyphicon glyphicon-plus"></span> Добавить новое поле
                </button>
            </form>

        </div>
    </div>
    <button v-on:click="createdCategory()" style="display: none" v-show="isShow" id="add-category-submit" type="button" class="btn btn-default btn-lg top-buffer">
        <span class="glyphicon glyphicon-plus"></span> Добавить
    </button>
</div>
@endsection

@section('scripts')
<script src="{{asset('/js/helper.js')}}"></script>
<script src="{{asset('/js/admin-catalog.js')}}"></script>
<script type="text/javascript">
    Vue.component('categories-list', {
        template:'\
                <div id="categories-table">\
                    <div v-for="(value, key) in categories">\
                        <a href="#">@{{ value }}</a>\
                    </div>\
                </div>',         
        data: function () {
            var cats = [];
            @foreach($categories as $category)
                cats[{{$category->id}}] = '{{$category->name}}'
            @endforeach
            console.log(cats);
            return {categories: cats};
        },
        created: function() {
            //тут в хуке при создании компонента ставим обработчик на ивент
            var vm = this;
            window.Event.$on('sendCategoryEvent', function(data){
                //который назначит массиву компонента переданную data
                if(data){
                    vm.categories.push(data['name']);
                }            
            });
        }
    });
    
    var Error = {
        template: '<span class="help-block">\
                            <slot></slot>\
                        </span>',
        props: ['errors']                                
       }
    
    window.Event = new Vue();
    
    var vm = new Vue({
        el: '#catalog-container',
        components: {
            'form-error': Error
        },
        data:{
            isShow:false,
            categoryName: null,
            categoryParentId: null,
            categoryDescription: null,
            categoryUrlPart: null,
            categoryActive: false,
            submitted: false,
            errors: [],
            successMessage: null,
            attributes: []
        },
        methods: {
            showAdd: function () {
                this.isShow = true;
            },
            hideAdd: function () {
                this.isShow = false;
            }, 
            createdCategory: function () {
                var data = new FormData();
                data.append( 'name', this.categoryName );
                data.append( 'parent_id', this.categoryParentId );
                data.append( 'description', this.categoryDescription );
                data.append( 'url_part', this.categoryUrlPart );
                data.append( 'active', this.categoryActive );                
                var attrJson = JSON.stringify(this.attributes);             
                data.append( 'additional', attrJson );
                data.append( '_token', window.Laravel.csrfToken );
                        
                fetch("{{route('addCategory')}}", {
                        method: 'post',
                        credentials: "same-origin",  
                        dataType: 'json',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                          },
                        body: JSON.stringify({
                            name: this.categoryName,
                            parent_id: this.categoryParentId,
                            description: this.categoryDescription,
                            url_part: this.categoryUrlPart,
                            active: this.categoryActive,
                            additional: attrJson,
                            _token: window.Laravel.csrfToken
                        }),
                    })
                    .then((response) => {
                        if(response.ok) {                         
                            console.log('success!');                        
                            return response.json();
                        }
                        return response.json();
                        //throw new Error('Network response was not ok');
                    })
                    .then((json) => {
                        if(json['status'] == 'ok'){
                            console.log(json);
                            this.hideAdd();
                            this.submitted = true;
                            this.successMessage = this.categoryName;
                            newCategory = {
                                id: json['id'],
                                name: json['name']
                            };
                            if(this.categoryActive){
                                sendCategory(newCategory);        
                            }
                            this.categoryName = null;
                            this.categoryParentId = null;
                            this.categoryDescription = null;
                            this.categoryUrlPart = null;
                            this.categoryActive = false;
                            this.attributes = [];
                            this.errors = [];
                        }
                        else{     
                            this.errors = json;
                            this.$set(this.errors,'',json);
                            console.log(json); 
                        }
                        
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            },    
            addNewField: function(e){
                e.preventDefault(); 
                if(this.attributes.length > 0){
                    if(!this.attributes[this.attributes.length - 1].name ||
                            !this.attributes[this.attributes.length - 1].type ||
                        !this.attributes[this.attributes.length - 1].system_alias){
                        alert('Заполните необходимые поля, прежде чем добавлять новые.');
                        return true;
                   }
                }
                this.attributes.push({ name: null, type: null, system_alias: null });
            }
        } 
    });
    function sendCategory(data) {
        //инициируем событие снаружи. прикрепляем к событию массив outsideData
        window.Event.$emit('sendCategoryEvent', data);
    }
</script>
@endsection